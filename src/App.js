import { Routes, Route } from 'react-router-dom';

import Navbar from './components/Navbar'


import CardsCharacters from './components/CardsCharacters'
import ListCharacters from './components/ListCharacters'
import Home from './components/Home'


function App() {
  return (
    <div className="flex-row">
      <Routes>
      <Route path="/" element={<Navbar />}>
        <Route path="cardCharacters" element={<CardsCharacters />} />
        <Route path="listCharacters" element={<ListCharacters />} />
        <Route path="*" element={<Home />} />
      </Route>
    </Routes>
    </div>
  );
}

export default App;
